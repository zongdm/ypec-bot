## Description

telegram timetable bot for educational institution

web scraping https://www.ypec.ru/

nodejs, pgsql, redis, google api, docker

## Running the app

```bash
# dev
$ npm run dev

# prod
$ npm run start
```

## Docker
1. Edit env value docker-compose.yml.example
2. Rename it to docker-compose.yml


```bash
docker compose up
```
