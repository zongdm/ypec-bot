import dotenv from 'dotenv'
dotenv.config({ path: __dirname + '/../.env' })
import { Context, Scenes, session, Telegraf } from 'telegraf'
import { createConnection } from 'typeorm'
import { pgsqlConfig } from './config/ormconfig'
import { groupScene, groupSceneWizard } from './controllers/group/group.controller'
import { startScene, startSceneWizard } from './controllers/start/start.controller'
import { raspStudScene, raspStudSceneWizard } from './controllers/rasp-student/rasp-stud.controller'
import { adminScene, adminSceneWizard } from './controllers/admin/admin.controller'
import { mailingSceneWizard } from './controllers/admin/sub-сontroller/mailing.controller'
import { updtgrpSceneWizard } from './controllers/admin/sub-сontroller/update-group.controller'
import { raspProfScene, raspProfSceneWizard } from './controllers/rasp-professor/rasp-professor.controller'

export interface MsgContext extends Context {
	// will be available under `variable`
	// declare scene type
	scene: Scenes.SceneContextScene<MsgContext, Scenes.WizardSessionData>
	// declare wizard type
	wizard: Scenes.WizardContextWizard<MsgContext>
}

async function bootstrap() {
	await createConnection(pgsqlConfig).catch((err) => console.error(err))

	const TOKEN = process.env.BOT_TOKEN
	if (TOKEN === undefined) {
		throw new Error('Token Error').message
	}

	const bot = new Telegraf<MsgContext>(TOKEN)

	const stage = new Scenes.Stage<MsgContext>([
		startSceneWizard,
		groupSceneWizard,
		raspStudSceneWizard,
		raspProfSceneWizard,
		adminSceneWizard,
		mailingSceneWizard,
		updtgrpSceneWizard,
	])
	bot.use(session(), stage.middleware())

	bot.command('start', startScene)
	bot.hears(/П|привет/, startScene)

	bot.command('group', groupScene)

	bot.command('rasp', raspStudScene)
	bot.hears(/Р|расписание/, raspStudScene)

	bot.command('professors', raspProfScene)

	bot.command('admin', adminScene)

	bot.launch()
	console.info('Bot start...')
}
bootstrap()
