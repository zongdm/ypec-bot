import { load } from 'cheerio'
import request from 'request-promise'

interface IOptions {
	method: string
	uri: string
}

export async function getAllGroup(): Promise<string[]> {
	const method = 'GET'
	const url = 'https://www.ypec.ru/rasp'
	const options: IOptions = { method, uri: url }
	const htmlreq = await request(options)
	const arrGroup: string[] = []
	const $ = load(htmlreq)
	$('select')
		.children()
		.each((i, el) => {
			const group = $(el).text().trim()
			if (!(group.length === 0)) {
				arrGroup.push(group)
			}
		})
	return arrGroup
}
