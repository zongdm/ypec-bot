// Проверить содержимое строки пары:
// Если есть дробь, не выводить время
// Если есть тире, брать первое число
function getFirstClass(classNum: string): number | undefined {
	const arrOfClassNum = classNum.split('')
	const lengthClassNum = arrOfClassNum.length
	if (lengthClassNum === 1) {
		return Number(classNum)
	}
	if (classNum.split('').includes('-')) {
		return Number(arrOfClassNum[0])
	}
	return undefined
}

//Кабинет может быть пустым значением(= 0) -- ничего не возвращаем
//Кабинет может быть дистант(дист) -- ничего не возвращаем
//Кабинет может быть спортинвый зал(с/з) -- время по 3 этажу
function getFloorCabinet(floorCabinet: string): number | undefined {
	if (floorCabinet.trim().length === 0 || floorCabinet === 'дист') {
		return undefined
	}
	floorCabinet = floorCabinet.split('-')[1]
	if (floorCabinet === 'с/з') {
		return 3
	}
	floorCabinet = floorCabinet.split('')[0]
	return Number(floorCabinet)
}

export function getClassBegin(classNum: string, floorCabinet: string, dayOfWeek: string): string | undefined {
	const firstClass = getFirstClass(classNum)
	const NumFloorCabinet = getFloorCabinet(floorCabinet)
	if (firstClass === undefined || NumFloorCabinet === undefined) {
		return undefined
	}

	switch (dayOfWeek) {
		case 'Суббота':
			switch (firstClass) {
				case 1:
					switch (NumFloorCabinet) {
						case 2:
						case 4:
							return '8:30'
						case 1:
						case 3:
							return '8:45'
					}
				case 2:
					switch (NumFloorCabinet) {
						case 2:
						case 4:
							return '10:10'
						case 1:
						case 3:
							return '10:15'
					}
				case 3:
					return '11:45'
				case 4:
					return '13:15'
				case 5:
					return '14:45'
				case 6:
					return '16:20'
			}
			break
		default:
			switch (firstClass) {
				case 1:
					switch (NumFloorCabinet) {
						case 2:
						case 4:
							return '8:30'
						case 1:
						case 3:
							return '8:45'
					}
				case 2:
					switch (NumFloorCabinet) {
						case 2:
						case 4:
							return '10:10'
						case 1:
						case 3:
							return '10:25'
					}
				case 3:
					switch (NumFloorCabinet) {
						case 2:
						case 4:
							return '12:10'
						case 1:
						case 3:
							return '12:25'
					}
				case 4:
					switch (NumFloorCabinet) {
						case 2:
						case 4:
							return '13:50'
						case 1:
						case 3:
							return '14:05'
					}
				case 5:
					return '15:30'
				case 6:
					return '17:00'
				case 7:
					return '18:30'
			}
			break
	}
}
