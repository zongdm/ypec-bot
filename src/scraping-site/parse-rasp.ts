import { CheerioAPI, load, Cheerio, Element } from 'cheerio'
import request from 'request-promise'

interface IOptions {
	method: string
	uri: string
	form: {
		grp: string
	}
}

interface IStableSheduleNR extends IShedule {
	numerator: boolean | undefined
}

export interface IShedule {
	classNum: string
	schedule: string
	cabinet: string
	teacher: string
}

export interface IJSONShedule {
	scheduleDate: string
	numerator: string
	scrapedShedule: IShedule[]
}

export async function getSchedule(group: string, date: 'сегодня' | 'завтра'): Promise<IJSONShedule | undefined> {
	try {
		const siteData = await getSiteData(group, date)
		return getScrapeHtmlData(siteData, group)
	} catch (err) {
		console.error(err)
	}
}

async function getSiteData(group: string, date: 'сегодня' | 'завтра') {
	try {
		const method = 'POST'
		const uri = getUrlByDate(date)
		const options: IOptions = { method, uri, form: { grp: group } }
		return await request(options)
	} catch (err: unknown) {
		if (err instanceof Error) {
			console.error(err.message)
		} else {
			console.error(err)
		}
	}
}

function getUrlByDate(date: 'сегодня' | 'завтра'): string {
	if (date === 'сегодня') {
		return 'https://www.ypec.ru/rasp-s?tmpl=component&print=1'
	}
	return 'https://www.ypec.ru/rasp-z?tmpl=component&print=1'
}

function getScrapeHtmlData(html: string, group: string): IJSONShedule {
	const $ = load(html)

	const replacementsScheduleTableFirstRowPosition = 'table.isp > tbody > tr:nth-child(2) > td'
	const replacementsScheduleTableRowPosition = 'table.isp > tbody > tr'
	const numeratorPosition = 'table.isp > tbody > tr > td > center > strong > font'
	const stableScheduleTableRowPosition = 'table.isp3 > tbody > tr.isp2 > td.isp_tec'
	const datePosition = 'table.isp > tbody > tr > td > center > strong > font'

	const numerator = getDateNumerator($, numeratorPosition, group)
	const scheduleDate = getScrapeDate($, datePosition)

	const isReplacementAllDay = getIsReplacementAllDay($, replacementsScheduleTableFirstRowPosition)

	if (isReplacementAllDay === true) {
		const scrapedShedule = getScrapeReplacementsSchedule($, replacementsScheduleTableRowPosition)
		console.log(scrapedShedule)
	} else {
		const scrapedReplacementsSchedule = getScrapeReplacementsSchedule($, replacementsScheduleTableRowPosition)
		const scrapedStableSheduleNR = getScrapeStableSchedule($, stableScheduleTableRowPosition)
		const scrapedStableShedule = getScrapeStableScheduleByNumerator(scrapedStableSheduleNR, numerator)
		getFinalSchedule(scrapedReplacementsSchedule, scrapedStableShedule)
	}

	// const scrapedReplacementsSchedule = getScrapeReplacementsSchedule($, replacementsScheduleTableRowPosition)
	// const scrapedStableSheduleNR = getScrapeStableSchedule($, stableScheduleTableRowPosition)
	// const scrapedStableShedule = getScrapeStableScheduleByNumerator(scrapedStableSheduleNR, numerator)

	//return { scheduleDate, numerator, scrapedShedule }
}

function getIsReplacementAllDay($: CheerioAPI, replacementsScheduleTableFirstRowPosition: string): boolean {
	const firstReplacementRow = $(replacementsScheduleTableFirstRowPosition).length
	if (firstReplacementRow === 4) {
		return true
	} else {
		return false
	}
}

function getScrapeReplacementsSchedule($: CheerioAPI, replacementsScheduleTableRowPosition: string): IShedule[] {
	const scrapedShedule: IShedule[] = []
	$(replacementsScheduleTableRowPosition).each((i, element) => {
		const columns = $(element).find('td')
		let classNum: string, schedule: string, replacements: string, cabinet: string, teacher: string
		let tableRow: IShedule
		switch (columns.length) {
			case 4:
				classNum = $(columns[0]).text().trim()
				schedule = $(columns[1]).text().trim()
				cabinet = $(columns[2]).text().trim()
				teacher = $(columns[3]).text().trim()
				tableRow = { classNum, schedule, cabinet, teacher }
				scrapedShedule.push(tableRow)
				break
			case 5:
				classNum = $(columns[0]).text().trim()
				schedule = $(columns[1]).text().trim()
				replacements = $(columns[2]).text().trim()
				cabinet = $(columns[3]).text().trim()
				teacher = $(columns[4]).text().trim()
				if (replacements.trim()) {
					schedule = replacements
				}
				tableRow = { classNum, schedule, cabinet, teacher }
				scrapedShedule.push(tableRow)
				break
		}
	})
	scrapedShedule.shift() // Удаляет заголовки таблицы

	return getReplacementsScheduleToSingleLine(scrapedShedule)
}

function getReplacementsScheduleToSingleLine(scrapedShedule: IShedule[]) {
	const t: IShedule[] = []
	scrapedShedule.forEach((value) => {
		if (value.classNum.includes('-')) {
			const b = value.classNum.split('-')
			const c: number[] = []
			let num = +b[0]
			do {
				t.push({
					classNum: num.toString(),
					schedule: value.schedule,
					cabinet: value.cabinet,
					teacher: value.teacher,
				})
				num++
			} while (+b[1] >= num)
		} else {
			t.push(value)
		}
	})
	return t
}

function getScrapeStableSchedule($: CheerioAPI, stableScheduleTableRowPosition: string): IStableSheduleNR[] {
	const scrapedStableSchedule: IStableSheduleNR[] = []

	let necessaryTable = $(stableScheduleTableRowPosition).parent()

	let classNum: string, schedule: string, cabinet: string, teacher: string

	let tableRow: IStableSheduleNR

	do {
		necessaryTable.each((i, element) => {
			const columns = $(element).find('td')

			const numerator = getTableRowNumerator(columns)

			// 5 - строка с днем недели
			// 3 - eсли к одной паре стоит две дисциплины, по числителю и знаменателю
			switch (columns.length) {
				case 5:
					classNum = $(columns[1]).text().trim()
					schedule = $(columns[2]).text().trim()
					teacher = $(columns[3]).text().trim()
					cabinet = $(columns[4]).text().trim()
					tableRow = { classNum, schedule, cabinet, teacher, numerator }
					scrapedStableSchedule.push(tableRow)
					break
				case 4:
					classNum = $(columns[0]).text().trim()
					schedule = $(columns[1]).text().trim()
					teacher = $(columns[2]).text().trim()
					cabinet = $(columns[3]).text().trim()
					tableRow = { classNum, schedule, cabinet, teacher, numerator }
					scrapedStableSchedule.push(tableRow)
					break
				case 3:
					classNum = scrapedStableSchedule[scrapedStableSchedule.length - 1].classNum
					schedule = $(columns[0]).text().trim()
					teacher = $(columns[1]).text().trim()
					cabinet = $(columns[2]).text().trim()
					tableRow = { classNum, schedule, cabinet, teacher, numerator }
					scrapedStableSchedule.push(tableRow)
					break
			}
		})
		necessaryTable = necessaryTable.next()
	} while (!necessaryTable.hasClass('isp2') && necessaryTable.get(0))

	return scrapedStableSchedule
}

function getScrapeStableScheduleByNumerator(schedule: IStableSheduleNR[], numerator: string): IShedule[] {
	const scrapedShedule: IShedule[] = []
	if (numerator === 'Числитель') {
		schedule.forEach((v) => {
			if (v.numerator !== false) {
				scrapedShedule.push({
					classNum: v.classNum,
					schedule: v.schedule,
					cabinet: v.cabinet,
					teacher: v.teacher,
				})
			}
		})
		return scrapedShedule
	} else {
		schedule.forEach((v) => {
			if (v.numerator !== true) {
				scrapedShedule.push({
					classNum: v.classNum,
					schedule: v.schedule,
					cabinet: v.cabinet,
					teacher: v.teacher,
				})
			}
		})
		return scrapedShedule
	}
}

function getFinalSchedule(replacementsSchedul: IShedule[], stableSchedule: IShedule[]) {
	const scrapedShedule: IShedule[] = []
	console.log(replacementsSchedul, stableSchedule)
	console.log(replacementsSchedul[0].classNum, stableSchedule[0].classNum)

	// Надем первую пару в расписании
}

function getTableRowNumerator(columns: Cheerio<Element>): boolean | undefined {
	const tagAttribute = columns[2].attribs
	if (tagAttribute.bgcolor) {
		if (tagAttribute.bgcolor.trim() === 'dfdfdf') {
			return true
		} else if (tagAttribute.bgcolor.trim() === 'a3a5a4') {
			return false
		}
	} else {
		return undefined
	}
}

function getDateNumerator($: CheerioAPI, numeratorPosition: string, group: string) {
	return $(numeratorPosition).text().split('-')[1].trim().replace(group, '')
}

function getScrapeDate($: CheerioAPI, datePosition: string) {
	return $(datePosition).text().split('-')[0].trim()
}

;(async () => {
	//20ДКП
	//21ЮП
	console.log(await getSchedule('18КС', 'завтра'))
})()
