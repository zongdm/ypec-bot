/*
	{
      "classNum": "4",
      "scheduled": "БЖ",
      "cabinet": "А-415",
      "teacher": "Нефедов А.Г."
    },


	`Расписание на ${scheduleDate} — ${group}`
	Расписание на 27.09.2021(Понедельник)
	18КС — Числитель

	Замены:
	6/6 + 2 | Техническое обслуживание и ремонт компьтерных сист — А415, Магнитский Р.С.

	Расписание:
	`${classNum} пара, ${scheduled} — ${cabinet}, ${teacher}`
	4 | БЖ — А415, Нефедов А.Г.
	5-6 | Техническое обслуживание и ремонт компьтерных сист — А415, Магнитский Р.С.

	Начало пар в ${classBeginText}
*/

import { createClient } from 'redis'
import { getClassBegin } from './class-begin'
import { getSchedule, IShedule } from './parse-rasp'
import { promisify } from 'util'

const siteError = 'Произошла ошибка, возможно сайт перестал отвечать 🤷\nПопробуйте позже...'

// Переработать кэш систему
export async function getStringSchedule(group: string, date: 'сегодня' | 'завтра'): Promise<string> {
	const client = createClient({
		host: process.env.REDIS_HOST,
		port: Number(process.env.REDIS_PORT),
	})
	const redisGet = promisify(client.get).bind(client)

	const cacheData = await redisGet(group + date)

	if (cacheData) {
		client.end(true)
		return cacheData
	} else {
		const schedule = await fetchSiteSchedule(group, date)
		if (schedule === siteError) {
			client.setex(group + date, 120, schedule)
		} else {
			if (date === 'сегодня') {
				client.setex(group + date, secondsToTomorrow(), schedule)
			} else {
				client.setex(group + date, 120, schedule)
			}
		}
		client.end(true)
		return schedule
	}
}

async function fetchSiteSchedule(group: string, date: 'сегодня' | 'завтра'): Promise<string> {
	const schedule = await getSchedule(group, date)
	if (schedule === undefined) {
		return siteError
	}

	const dateText = `Расписание на ${schedule.scheduleDate}\n${group} — ${schedule.numerator}\n\n`

	// Замен нет
	// if (schedule.scrapedShedule.length === 0) {
	// 	return dateText + raspEmpty
	// }

	// Отдельная фукнция для просчета времени
	// const firstClass = schedule.scrapedShedule[0].classNum
	// const floorCabinet = schedule.scrapedShedule[0].cabinet
	// const dayOfWeek = schedule.scheduleDate.split('(')[1].trim().slice(0, -1)
	// const classBegin = getClassBegin(firstClass, floorCabinet, dayOfWeek)

	// Замен нет или Замены: Расписание

	const replacementsScheduleText = schedule.scrapedReplacementsSchedule
		.map((value) => {
			return `${value.classNum} | ${value.schedule} — ${value.cabinet}, ${value.teacher}`
		})
		.join('\n')

	const stableScheduleText = schedule.scrapedStableShedule
		.map((value) => {
			return `${value.classNum} | ${value.schedule} — ${value.cabinet}, ${value.teacher}`
		})
		.join('\n')

	// if (classBegin === undefined) {
	// 	return dateText + raspText
	// }
	// const classBeginText = `\n\nПары начинаются в ${classBegin}`

	return dateText + 'Замены:\n' + replacementsScheduleText + '\n\nРасписание:\n' + stableScheduleText // + classBeginText
}

function replacementsScheduleText(scrapedReplacementsSchedule: IShedule[]) {
	if (scrapedReplacementsSchedule.length === 0) {
		return 'Замен нет'
	}

	scrapedReplacementsSchedule
		.map((value) => {
			return `${value.classNum} | ${value.schedule} — ${value.cabinet}, ${value.teacher}`
		})
		.join('\n')
}

function secondsToTomorrow() {
	const secondsInDay = 86400
	const now = new Date()
	const seconsLeftToday = (now.getHours() * 60 + now.getMinutes()) * 60 + now.getSeconds()
	return secondsInDay - seconsLeftToday
}
