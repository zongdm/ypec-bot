import { PostgresConnectionOptions } from 'typeorm/driver/postgres/PostgresConnectionOptions'
import { Admin } from '../entity/admin.entity'
import { Group } from '../entity/group.entity'
import { Professor } from '../entity/professor.entity'
import { User } from '../entity/user.entity'

export const pgsqlConfig: PostgresConnectionOptions = {
	type: 'postgres',
	host: process.env.PGSQL_HOST,
	port: Number(process.env.PGSQL_PORT),
	username: process.env.PGSQL_USERNAME,
	password: process.env.PGSQL_PASSWORD,
	database: process.env.PGSQL_DATABASE,
	entities: [User, Group, Admin, Professor],
	logging: true,
	synchronize: true,
}
