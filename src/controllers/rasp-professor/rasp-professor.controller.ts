import { Composer, Markup, Scenes } from 'telegraf'
import { getRepository, Like } from 'typeorm'
import { MsgContext } from '../../bot'
import { Professor } from '../../entity/professor.entity'
import { rasp_prof_exit_keyboard } from './rasp-professor.keyboard'

const raspProfMessage = async (ctx: MsgContext) => {
	await ctx.reply('Ввидите препадователя или его инициалы', rasp_prof_exit_keyboard)
	ctx.wizard.next()
}

const raspProfChangeHandler = new Composer<MsgContext>()
raspProfChangeHandler.on('text', async (ctx) => {
	const professorRepository = getRepository(Professor)

	const professormsg = ctx.message.text.trim()
	const foundProfessors = (
		await professorRepository.find({ where: { DetailedFullName: Like(`%${professormsg}%`) } })
	).map((v) => [v.DetailedFullName])

	if (foundProfessors.length === 0) {
		return await ctx.reply('Ничего не найдено', rasp_prof_exit_keyboard)
	}

	if (foundProfessors.length === 1) {
		await ctx.reply(foundProfessors[0][0])
		await ctx.scene.enter('raspStudScene')
	}

	const foud_prof_keyboard = Markup.keyboard(foundProfessors).resize()
	await ctx.reply(
		'😜 Данный раздел в разработке...\nЧуть позже тут можно будет узнать расписание\nНашел:',
		foud_prof_keyboard
	)
	setTimeout(async () => {
		await ctx.scene.enter('raspStudScene')
	}, 5000)
})

export const raspProfSceneWizard = new Scenes.WizardScene<MsgContext>(
	'raspProfScene',
	raspProfMessage,
	raspProfChangeHandler
)

export const raspProfScene = (ctx: MsgContext) => {
	ctx.scene.enter('raspProfScene')
}
