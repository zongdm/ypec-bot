import { Markup } from 'telegraf'

export const start_inline_keyboard = Markup.inlineKeyboard([
	[Markup.button.callback('Я студент 🎓', 'studentTrue')],
	[Markup.button.callback('Нет', 'studentFalse')],
])

export const site_button = Markup.inlineKeyboard([
	[Markup.button.url('Сайт', 'https://www.ypec.ru/')],
	[Markup.button.url('Группа VK', 'https://vk.com/ypecnews')],
	[Markup.button.url('Абитуриенту', 'https://www.ypec.ru/abiturientu')],
])
