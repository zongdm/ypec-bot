import { Composer, Markup, Scenes } from 'telegraf'
import { getRepository } from 'typeorm'
import { MsgContext } from '../../bot'
import { Group } from '../../entity/group.entity'
import { User } from '../../entity/user.entity'
import { site_button, start_inline_keyboard } from './start.keyboard'

const startMessage = async (ctx: MsgContext) => {
	const userRepository = getRepository(User)
	const oldUser = await userRepository.findOne({ where: { id: ctx.from?.id }, relations: ['group'] })
	if (oldUser) {
		const group = oldUser.group.name
		await ctx.reply(`👋 Ты указал, что являешься студентом группы ${group}`, Markup.removeKeyboard())
		await ctx.scene.enter('raspStudScene')
	} else {
		await ctx.reply('👋 Я бот колледжа ЯПЭК, давай знакомиться. Ты студент?', start_inline_keyboard)
		ctx.wizard.next()
	}
}

const startButtonHandler = new Composer<MsgContext>()
startButtonHandler.action('studentTrue', async (ctx) => {
	await ctx.answerCbQuery()
	await ctx.editMessageReplyMarkup(undefined)
	ctx.scene.enter('groupScene')
})
startButtonHandler.action('studentFalse', async (ctx) => {
	await ctx.answerCbQuery()
	await ctx.editMessageReplyMarkup(undefined)
	await ctx.editMessageText('😕 Я был разработан в помощь студентам. Возможно, ты ищешь...', site_button)
	await ctx.scene.leave()
})

export const startSceneWizard = new Scenes.WizardScene<MsgContext>('startScene', startMessage, startButtonHandler)

export const startScene = (ctx: MsgContext) => {
	ctx.scene.enter('startScene')
}
