import { Markup } from 'telegraf'

export const main_rasp_keyboard = Markup.keyboard([
	['Расписание на сегодня'],
	['Расписание на завтра'],
	['Регламент учебных занятий'],
]).resize()
