import { Composer, Markup, Scenes } from 'telegraf'
import { getRepository } from 'typeorm'
import { MsgContext } from '../../bot'
import { User } from '../../entity/user.entity'
import { getStringSchedule } from '../../scraping-site/rasp-to-string'
import { main_rasp_keyboard } from './rasp.keyboard'

const raspStudMessage = async (ctx: MsgContext) => {
	ctx.reply('Выберете действие', main_rasp_keyboard)
	ctx.wizard.next()
}

const raspStudMessageHandler = new Composer<MsgContext>()
raspStudMessageHandler.hears('Расписание на сегодня', async (ctx) => {
	const userRepository = getRepository(User)
	const user = await userRepository.findOne({ where: { id: ctx.from.id }, relations: ['group'] })
	if (!user) {
		return await ctx.scene.enter('groupScene')
	}
	const userGroup = user.group.name
	ctx.reply(await getStringSchedule(userGroup, 'сегодня'))
})

raspStudMessageHandler.hears('Расписание на завтра', async (ctx) => {
	const userRepository = getRepository(User)
	const user = await userRepository.findOne({ where: { id: ctx.from.id }, relations: ['group'] })
	if (!user) {
		return await ctx.scene.enter('groupScene')
	}
	const userGroup = user.group.name
	ctx.reply(await getStringSchedule(userGroup, 'завтра'))
})

raspStudMessageHandler.hears('Регламент учебных занятий', async (ctx) => {
	if (process.env.REGL) {
		await ctx.replyWithPhoto(process.env.REGL)
	}
})

export const raspStudSceneWizard = new Scenes.WizardScene<MsgContext>(
	'raspStudScene',
	raspStudMessage,
	raspStudMessageHandler
)

export const raspStudScene = (ctx: MsgContext) => {
	ctx.scene.enter('raspStudScene')
}
