import { Markup } from 'telegraf'

export const main_admin_keyboard = Markup.keyboard([['Статистика'], ['Сделать рассылку'], ['Обновить данные']]).resize()

export const refresh_admin_keyboard = Markup.keyboard([
	['Обновить учебные группы в БД'],
	['Обновить таблицу преподавателей'],
	['Обновить БД преподавателей'],
]).resize()
