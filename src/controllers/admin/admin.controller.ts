import { Composer, Markup, Scenes } from 'telegraf'
import { getRepository } from 'typeorm'
import { MsgContext } from '../../bot'
import { Admin } from '../../entity/admin.entity'
import { Professor } from '../../entity/professor.entity'
import { updateProfessorsDB } from '../../google-sheet/update-professors-db'
import { updateProfessorsSheet } from '../../google-sheet/update-professors-sheet'
import { main_admin_keyboard, refresh_admin_keyboard } from './admin.keyboard'
import { statsScene } from './sub-сontroller/stats.controller'

const adminMessage = async (ctx: MsgContext) => {
	await ctx.reply('Admin: Выберете действия', main_admin_keyboard)
	ctx.wizard.next()
}

const adminMessageHandler = new Composer<MsgContext>()
adminMessageHandler.hears('Обновить данные', async (ctx) => {
	await ctx.reply('Admin: Какие данные вы хотите обновить?', refresh_admin_keyboard)
})

adminMessageHandler.hears('Сделать рассылку', async (ctx) => {
	await ctx.scene.enter('mailingScene')
})

adminMessageHandler.hears('Статистика', async (ctx) => {
	await statsScene(ctx)
})

adminMessageHandler.hears('Обновить учебные группы в БД', async (ctx) => {
	await ctx.scene.enter('updtgrpScene')
})

adminMessageHandler.hears('Обновить таблицу преподавателей', async (ctx) => {
	await ctx.reply('Admin: Началось обновление таблицы...')
	const sheetMsg = await updateProfessorsSheet()
	await ctx.reply(`Admin: ${sheetMsg}`)
	await ctx.scene.enter('raspStudScene')
})

adminMessageHandler.hears('Обновить БД преподавателей', async (ctx) => {
	await ctx.reply('Admin: Началось обновление БД...')
	const dbMsg = await updateProfessorsDB()
	await ctx.reply(`Admin: ${dbMsg}`)
	await ctx.scene.enter('raspStudScene')
})

export const adminSceneWizard = new Scenes.WizardScene<MsgContext>('adminScene', adminMessage, adminMessageHandler)

export const adminScene = async (ctx: MsgContext) => {
	const adminRepository = getRepository(Admin)
	const isAdmin = await adminRepository.findOne({ where: { id: ctx.from?.id } })
	if (isAdmin) {
		return ctx.scene.enter('adminScene')
	}
	ctx.reply('Доступ запрещен 🔒')
}
