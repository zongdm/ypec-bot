import { Composer, Markup, Scenes } from 'telegraf'
import { getRepository } from 'typeorm'
import { MsgContext } from '../../../bot'
import { Group } from '../../../entity/group.entity'
import { User } from '../../../entity/user.entity'
import { getAllGroup } from '../../../scraping-site/parse-group'

const valid_inline_keyboard = Markup.inlineKeyboard([
	[Markup.button.callback('Да', 'updtTrue')],
	[Markup.button.callback('Нет', 'updtFalse')],
])

const updtgrpMessage = async (ctx: MsgContext) => {
	await ctx.reply('Admin: ДАЛЬНЕЙШИЕ ДЕЙСТВИЯ СОТРУТ БАЗУ ДАННЫХ ПОЛЬЗОВАТЕЛЕЙ', Markup.removeKeyboard())
	await ctx.reply('Admin: Вы действительно хотите обновить группы?', valid_inline_keyboard)
	ctx.wizard.next()
}

const updtgrpMessageHandler = new Composer<MsgContext>()
updtgrpMessageHandler.action('updtTrue', async (ctx) => {
	try {
		await ctx.answerCbQuery()
		await ctx.editMessageReplyMarkup(undefined)

		const userRepository = getRepository(User)
		await userRepository.clear()

		const groupRepository = getRepository(Group)
		const arrGroup = await getAllGroup()
		arrGroup.forEach(async (group) => {
			const newGroup = groupRepository.create({ name: group })
			await groupRepository.save(newGroup)
		})
		await ctx.reply('Список групп успешно обновлен')
	} catch (err) {
		await ctx.reply('Обновление завершилось с ошибкой\n\n' + err)
		console.error(err)
	} finally {
		await ctx.scene.enter('raspStudScene')
	}
})

updtgrpMessageHandler.action('updtFalse', async (ctx) => {
	await ctx.answerCbQuery()
	await ctx.editMessageReplyMarkup(undefined)
	await ctx.reply('Вы отменили свои действия')
	await ctx.scene.enter('raspStudScene')
})

export const updtgrpSceneWizard = new Scenes.WizardScene<MsgContext>(
	'updtgrpScene',
	updtgrpMessage,
	updtgrpMessageHandler
)

export const updtgrpgScene = (ctx: MsgContext) => {
	ctx.scene.enter('updtgrpScene')
}
