import { Markup } from 'telegraf'
import { getRepository } from 'typeorm'
import { MsgContext } from '../../../bot'
import { Admin } from '../../../entity/admin.entity'
import { User } from '../../../entity/user.entity'

export const statsScene = async (ctx: MsgContext) => {
	const userRepository = getRepository(User)
	const totalUser = await userRepository.count()
	const adminRepository = getRepository(Admin)
	const totalAdmin = await adminRepository.count()
	const adminUsername = (await adminRepository.find({ select: ['username'] })).map((v) => v.username).join(', ')
	if (ctx.message) {
		await ctx.reply(
			`Количество зарегистрированных пользователей: ${totalUser}\n` +
				`Администраторов: ${totalAdmin} (${adminUsername})\n` +
				`Сообщений: ${ctx.message.message_id}`,
			Markup.removeKeyboard()
		)
	}
	await ctx.scene.enter('raspStudScene')
}
