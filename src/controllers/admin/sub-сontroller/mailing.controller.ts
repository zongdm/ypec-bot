import { Composer, Markup, Scenes } from 'telegraf'
import { getRepository } from 'typeorm'
import { MsgContext } from '../../../bot'
import { User } from '../../../entity/user.entity'

const mailing_exit_keyboard = Markup.keyboard(['Выйти']).resize()

const mailingMessage = async (ctx: MsgContext) => {
	await ctx.reply('Admin: Какое сообщение вы хотите отправить?', mailing_exit_keyboard)
	ctx.wizard.next()
}

const mailMessageHandler = new Composer<MsgContext>()
mailMessageHandler.hears('Выйти', async (ctx) => {
	await ctx.reply('Admin: Вы вышли из рассылки', Markup.removeKeyboard())
	await ctx.scene.leave()
})

mailMessageHandler.on('photo', async (ctx) => {
	try {
		const userRepository = getRepository(User)
		const userId = (await userRepository.find({ select: ['id'] })).map((v) => v.id)
		const photo = ctx.update.message.photo[0].file_id
		const caption = ctx.update.message.caption
		for (let id of userId) {
			setTimeout(async () => {
				await ctx.telegram.sendPhoto(id, photo, { caption })
			}, 50 * id)
		}
		await ctx.reply('Admin: Рассылка прошла успешна')
	} catch (err) {
		await ctx.reply('Admin: Рассылка завершилась с ошибкой\n\n' + err)
		console.error(err)
	} finally {
		await ctx.scene.enter('raspStudScene')
	}
})

mailMessageHandler.on('text', async (ctx) => {
	try {
		const userRepository = getRepository(User)
		const userId = (await userRepository.find({ select: ['id'] })).map((v) => v.id)
		const text = ctx.message.text
		for (let id of userId) {
			setTimeout(async () => {
				await ctx.telegram.sendMessage(id, text)
			}, 50 * id)
		}
		await ctx.reply('Admin: Рассылка прошла успешна')
	} catch (err) {
		await ctx.reply('Admin: Рассылка завершилась с ошибкой\n\n' + err)
		console.error(err)
	} finally {
		await ctx.scene.enter('raspStudScene')
	}
})

export const mailingSceneWizard = new Scenes.WizardScene<MsgContext>('mailingScene', mailingMessage, mailMessageHandler)

export const mailingScene = (ctx: MsgContext) => {
	ctx.scene.enter('mailingScene')
}
