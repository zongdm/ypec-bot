import { Composer, Markup, Scenes } from 'telegraf'
import { getRepository } from 'typeorm'
import { MsgContext } from '../../bot'
import { Group } from '../../entity/group.entity'
import { User } from '../../entity/user.entity'
import { group_exit_keyboard } from './group.keyboard'

const groupMessage = async (ctx: MsgContext) => {
	const userRepository = getRepository(User)
	const user = await userRepository.findOne({ where: { id: ctx.from?.id }, relations: ['group'] })
	if (!user) {
		await ctx.reply('Пожалуйста, укажи номер своей группы')
		await ctx.reply('Ожидаю сообщение...', group_exit_keyboard)
		ctx.wizard.next()
	} else {
		await ctx.reply(`Сейчас ты записан как студент группы ${user.group.name}`)
		await ctx.reply('Ожидаю сообщение...', group_exit_keyboard)
		ctx.wizard.next()
	}
}

const groupChangeHandler = new Composer<MsgContext>()
groupChangeHandler.on('text', async (ctx) => {
	const userRepository = getRepository(User)
	const groupRepository = getRepository(Group)

	const groupmsg = ctx.message.text.trim().toUpperCase()
	const groupname = await groupRepository.findOne(groupmsg)
	if (!groupname) {
		return await ctx.reply('Такой группы не существует. Попробуй еще раз...')
	}

	const user = await userRepository.findOne({ where: { id: ctx.from?.id }, relations: ['group'] })
	if (!user) {
		const newUser = userRepository.create({
			id: ctx.from.id,
			group: groupname,
		})
		await userRepository.save(newUser)
		await ctx.reply('Группа успешна сохранена, вы сможете изменить ее по команде — /group', Markup.removeKeyboard())
		await ctx.scene.enter('raspStudScene')
	} else {
		user.group = groupname
		await userRepository.save(user)
		await ctx.reply('Группа успешна обновлена', Markup.removeKeyboard())
		await ctx.scene.enter('raspStudScene')
	}
})

export const groupSceneWizard = new Scenes.WizardScene<MsgContext>('groupScene', groupMessage, groupChangeHandler)
groupSceneWizard.hears('Выйти', async (ctx) => {
	ctx.reply('Вы отменили действие', Markup.removeKeyboard())
	await ctx.scene.leave()
})

export const groupScene = (ctx: MsgContext) => {
	ctx.scene.enter('groupScene')
}
