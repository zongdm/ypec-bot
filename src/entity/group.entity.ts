import { Entity, PrimaryColumn } from 'typeorm'

@Entity({ name: 'Groups' })
export class Group {
	@PrimaryColumn({ unique: true })
	name: string
}
