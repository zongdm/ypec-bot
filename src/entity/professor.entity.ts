import { Column, Entity, PrimaryColumn } from 'typeorm'

@Entity({ name: 'Professors' })
export class Professor {
	@PrimaryColumn({ unique: true })
	DetailedFullName: string

	@Column()
	FullName: string
}
