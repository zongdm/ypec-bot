import { CreateDateColumn, Entity, ManyToOne, PrimaryColumn } from 'typeorm'
import { Group } from './group.entity'

@Entity({ name: 'Users' })
export class User {
	@PrimaryColumn({ unique: true })
	id: number

	@ManyToOne(() => Group, (groups) => groups.name) // { onDelete: 'SET NULL', onUpdate: 'SET NULL' })
	group: Group

	@CreateDateColumn()
	createdAt: Date
}
