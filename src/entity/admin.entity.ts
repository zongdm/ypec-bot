import { Column, CreateDateColumn, Entity, PrimaryColumn } from 'typeorm'

@Entity({ name: 'Admins' })
export class Admin {
	@PrimaryColumn({ unique: true })
	id: number

	@Column()
	username: string

	@CreateDateColumn()
	createdAt: Date
}
