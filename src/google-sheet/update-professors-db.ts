import dotenv from 'dotenv'
dotenv.config({ path: __dirname + '/../../.env' })

import { getRepository, ILike, Like } from 'typeorm'
import { Professor } from '../entity/professor.entity'
import { googleAuthorize } from './google-auth'

export async function updateProfessorsDB() {
	try {
		const professorRepository = getRepository(Professor)

		const { auth, spreadsheetId, googleSheets } = await googleAuthorize()
		const rowsForDB = (
			await googleSheets.spreadsheets.values.get({
				auth,
				spreadsheetId,
				range: 'Лист1!A2:B',
			})
		).data.values

		await professorRepository.clear()

		if (rowsForDB) {
			for (let teacher of rowsForDB) {
				const newProfessor = professorRepository.create({ DetailedFullName: teacher[0], FullName: teacher[1] })
				await professorRepository.save(newProfessor)
			}
		}

		return 'Данные в БД обновлены'
	} catch (err) {
		console.error(err)
		return `Обновление завершилось с ошибкой:\n${err}`
	}
}
// const find = await professorRepository.find({ DetailedFullName: Like(`%${}%`) })
// console.log(find)
