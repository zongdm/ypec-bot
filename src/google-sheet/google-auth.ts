import { google } from 'googleapis'

export async function googleAuthorize() {
	const auth = new google.auth.GoogleAuth({
		keyFile: './google-credentials.json',
		scopes: 'https://www.googleapis.com/auth/spreadsheets',
	})
	const client = await auth.getClient()

	const spreadsheetId = '1CxPIkxosnv-jL5aLB4KZgBXPsz74VWP3-prPyj8bXvk'
	const googleSheets = google.sheets({ version: 'v4', auth: client })

	return { auth, spreadsheetId, googleSheets }
}
