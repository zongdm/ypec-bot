import { load } from 'cheerio'
import request from 'request-promise'
import { googleAuthorize } from './google-auth'

interface IOptions {
	method: string
	uri: string
}

export async function updateProfessorsSheet() {
	try {
		const professors = await siteReq()

		const diffSheet = await checkSheets(professors)
		if (diffSheet === undefined) {
			return `Google Sheet с преподавателями успешно обновлен, изменений не найдено.`
		} else {
			return `Google Sheet с преподавателями обновлен. Обнаружено несходство на ${diffSheet} строке.`
		}
	} catch (err) {
		console.error(err)
		return `Запрос завершился с ошибкой:\n${err}`
	}
}

async function siteReq() {
	const method = 'GET'
	const uri = 'https://www.ypec.ru/rasp-sp'
	const options: IOptions = { method, uri }
	const htmlreq = await request(options)
	const $ = load(htmlreq)
	const professors: string[][] = []
	$('select')
		.children()
		.each((i, el) => {
			const teacher = $(el).text().trim()
			if (!(teacher.length === 0)) {
				professors.push([teacher])
			}
		})
	return professors
}

async function checkSheets(professors: string[][] = []) {
	const { auth, spreadsheetId, googleSheets } = await googleAuthorize()

	await googleSheets.spreadsheets.values.update({
		auth,
		spreadsheetId,
		range: 'Лист1!C2:C',
		valueInputOption: 'RAW',
		requestBody: { values: professors },
	})

	const siteDataRows = (
		await googleSheets.spreadsheets.values.get({
			auth,
			spreadsheetId,
			range: 'Лист1!C2:C',
		})
	).data.values

	const currentDataRows = (
		await googleSheets.spreadsheets.values.get({
			auth,
			spreadsheetId,
			range: 'Лист1!B2:B',
		})
	).data.values

	for (let index in siteDataRows) {
		if (currentDataRows && currentDataRows[+index] !== undefined) {
			if (siteDataRows[+index][0] !== currentDataRows[+index][0]) {
				return +index + 2
			}
		} else {
			return +index + 2
		}
	}
}
