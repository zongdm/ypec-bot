FROM node:14

WORKDIR /app

COPY package*.json ./

RUN npm install typescript -g
RUN npm install

COPY . .

RUN npm run build

RUN rm -rf src

CMD [ "npm", "run", "start"]